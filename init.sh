#!/usr/bin/env bash

export LANG=C
export GROUP=TMW2
export ALTER=evol
export TEAM=fantasy-world-online

CMD="$1"
PROTO="git@gitlab.com:"
if [[ -z "${CMD}" ]]; then
    export CMD="default"
fi

STR=$(git remote -v|grep "https://")
if [[ -n "${STR}" ]]; then
    export PROTO="https://gitlab.com/"
fi

CLONECMD="git clone --origin upstream"
CLONE1="${CLONECMD} ${PROTO}${GROUP}"
CLONE2="${CLONECMD} ${PROTO}"
CLONEA="${CLONECMD} ${PROTO}${ALTER}"
CLONE="${CLONECMD} ${PROTO}${TEAM}"

if [[ "${CMD}" == "all" || "${CMD}" == "default" ]]; then
    ${CLONE}/clientdata.git client-data
    ${CLONEA}/hercules.git server-code
    ${CLONE}/serverdata.git server-data
    ${CLONE}/evol-tools.git tools
    ${CLONE}/evol-hercules.git server-code/src/evol
    ${CLONEA}/evol-local.git server-local # Do this even serve any purpose?
    ${CLONE}/tmw2-docs.git docs
    ${CLONE1}/Docs.wiki.git wiki
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "server" ]]; then
    ${CLONEA}/hercules.git server-code
    ${CLONE}/serverdata.git server-data
    ${CLONE}/evol-hercules.git server-code/src/evol
    ${CLONE}/evol-tools.git tools
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "client" ]]; then
    ${CLONE}/clientdata.git client-data
    ${CLONE}/evol-tools.git tools
elif [[ "${CMD}" == "music" ]]; then
    ${CLONE}/evol-music.git music
elif [[ "${CMD}" == "local" ]]; then
    ${CLONEA}/evol-local.git server-local
    ${CLONEA}/hercules.git server-code
    ${CLONE}/serverdata.git server-data
    ${CLONE}/evol-hercules.git server-code/src/evol
    ${CLONE}/evol-tools.git tools
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "tools" ]]; then
    ${CLONE}/evol-tools.git tools
elif [[ "${CMD}" == "plugin" ]]; then
    ${CLONE}/evol-hercules.git server-code/src/evol
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "docs" ]]; then
    ${CLONE}/tmw2-docs.git docs
    ${CLONE1}/Docs.wiki.git wiki
elif [[ "${CMD}" == "manaplus" ]]; then
    ${CLONE2}manaplus/manaplus.git manaplus
elif [[ "${CMD}" == "media" ]]; then
    ${CLONE1}/art.git art
elif [[ "${CMD}" == "mods" ]]; then
    ${CLONE1}/mods.git client-data/mods
elif [[ "${CMD}" == "update" ]]; then
    ${CLONEA}/hercules.git server-code
    ${CLONE}/evol-hercules.git server-code/src/evol
    ln -s server-code/src/evol server-plugin
fi

if [[ "${CMD}" == "all" ]]; then
    ${CLONE}/evol-music.git music
    ${CLONE1}/mods.git client-data/mods
    ln -s music/music client-data/
    ${CLONE2}manaplus/manaplus.git manaplus
fi
